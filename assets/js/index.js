import Router from './Router.js';
import { loaderPage } from './loader.js';
import { loaderPageBis } from './loader.js';
import { generateTable } from './tablegenerate.js';
import { resetValT } from './tablegenerate.js';
import { generateTableScore } from './tablescore.js';
import './navbar.js';
import './hangman.js';
import { chronometre } from './chrono.js';
import { notif } from './notif.js';
import { boutonClick } from './hangman.js';
import { resetValH } from './hangman.js';
import { wordReload } from './wordpromise.js';
import { processFile } from './filereader.js';

// Loader de la page a chaque changement
loaderPage();

// Charge la vue (hash correspond a l'"ancre dans l'url")
function loadview(hash) {
    const id = 'view';
    var router = new Router();
    router.addroute("home", 'pages/home.html', id);
    router.addroute("page1", 'pages/page1.html', id);
    router.addroute("page2", 'pages/page2.html', id);
    router.addroute("page3", 'pages/page3.html', id);
    router.router(hash)
}

//Init au debut
loadview();


// Event listener du menu generale (change de page)
document.getElementById('list').addEventListener('click', event => {
    // Loader
    loaderPageBis();
    // Si PAGE 1 (celle du jeu)
    if (event.target.attributes[1].value.slice(1) == 'page1') {
        // Vérification de l'existance d'un pseudo user
        if (sessionStorage.getItem("person")) {
            document.getElementById("pseudo").innerHTML = "Pseudo : " + sessionStorage.getItem("person");
        } else {
            var person = prompt("Rentrer un pseudo", "User");
            if (person != null) {
                sessionStorage.setItem('person', person);
                document.getElementById("pseudo").innerHTML = "Pseudo : " + sessionStorage.getItem("person");
            } else {
                alert('merci de rentrer un pseudo');
            }
        }
        // Génération du mot (cadriage) et lancement du chrono
        setTimeout(function () { generateTable(); }, 1500);
        setTimeout(function () { chronometre(); }, 3500);
        setTimeout(function () { boutonClick(); }, 1500);
        setTimeout(function () { reloadAfter(); }, 4500);


    }
    // Si page du score
    if (event.target.attributes[1].value.slice(1) == 'page2') {
        setTimeout(function () { generateTableScore(); }, 1500);
    } if (event.target.attributes[1].value.slice(1) == 'page3') {
        setTimeout(function () {
            document.getElementById('input').addEventListener('change', (e) => {
                const file = document.getElementById('input').files[0];
                if (file) {
                    processFile(e,file);
                }
            })
        }, 1500);
    }
    // Chargement de la vue
    loadview(event.target.attributes[1].value.slice(1));
})

// Initi score table json
const top = {
    '1': {
        'player': "UNDEFINED",
        'score': "0",
        'data': {
            'tentative': '0',
            'motlenght': '0',
            'time': '0',
            'word': '0'
        }
    },
    '2': {
        'player': "UNDEFINED",
        'score': "0",
        'data': {
            'tentative': '0',
            'motlenght': '0',
            'time': '0',
            'word': '0'
        }
    },
    '3': {
        'player': "UNDEFINED",
        'score': "0",
        'data': {
            'tentative': '0',
            'motlenght': '0',
            'time': '0',
            'word': '0'
        }
    }
}
// Local storage score board
if (sessionStorage.getItem("top")) {
    // on test la variabe des scores pour etre sur
    let testsessionStorage = sessionStorage.getItem("top");
    if (testsessionStorage == "undefined") {
        sessionStorage.setItem('top', JSON.stringify(top));
    }
} else {
    sessionStorage.setItem('top', JSON.stringify(top));

}

// Service worker notif au lancement
notif();

function reloadAfter() {
    document.getElementById('reloadnoreload').addEventListener('click', event => {
        wordReload();
        resetValH(); resetValT();
        setTimeout(function () { generateTable(); }, 1500);
        setTimeout(function () { chronometre(); }, 3500);
        setTimeout(function () { boutonClick(); }, 1500);
    })
}

