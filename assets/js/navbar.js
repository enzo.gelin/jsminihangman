// Reset pseudo inscrit au debut
function resetPseudo() {
  sessionStorage.removeItem('person');
  document.getElementById("pseudo").innerHTML = "Pseudo : UNDEFINED";

}
document.getElementById('accountcancel').addEventListener('click', resetPseudo)


// Active link menu
var header = document.getElementById("list");
var btns = header.getElementsByClassName("nav-link");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function () {
    var current = document.getElementsByClassName("active");
    if (current.length > 0) {
      current[0].className = current[0].className.replace(" active", "");
    }
    this.className += " active";
  });
}

// Get level batterie easy
navigator.getBattery().then(function (battery) {
  var level = battery.level;
  document.getElementById("batt").innerHTML = "Batterie sur le PC : " + Math.round(level * 100) + " %";
  console.log(level);
});



