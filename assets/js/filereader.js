

export const processFile = (file) => {
    debugger;
    const fr = new FileReader();

    fr.readAsDataURL(file);
    fr.onerror = errorHandler;
    fr.onabort = () => changeStatus('En cours de téléchargement');
    fr.onloadstart = () => changeStatus('En cours de téléchargement');
    fr.onload = () => { changeStatus('Téléchargé') };
    fr.onloadend = () => loaded;
    fr.onprogress = setProgress;
}

// Updates the value of the progress bar
const setProgress = (e) => {
    // The target is the file reader
    const fr = e.target;
    const loadingPercentage = 100 * e.loaded / e.total;
    document.getElementById('progress-bar').value = loadingPercentage;
}

const changeStatus = (status) => {
    document.getElementById('status').innerHTML = status
}

const loaded = (e) => {
    changeStatus('Téléchargment réussi');
    const fr = e.target
    var result = fr.result;
    console.log('result: '+result)
}

const errorHandler = (e) => {
    changeStatus("Erreurr: " + e.target.error.name)
}
