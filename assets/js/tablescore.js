import { prop_access } from './fonctionscours.js';


export function generateTableScore() {
    // Return objec size
    Object.size = function (obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };


    document.getElementById("scorestable").innerHTML = "";
    const root = document.getElementById("scorestable");
    const table = document.createElement("table");
    const tbody = document.createElement("tbody");

    // Recup score tab
    var top = JSON.parse(sessionStorage.getItem('top'));
    var lengthObj = Object.size(top);
    // Affichage score
    for (let i = 0; i < lengthObj + +1; i++) {
        const tr = document.createElement("tr");
        if(i == 0){
            for (let j = 0; j < 5; j++) {
                const td = document.createElement("td");
                switch (j) {
                    case 0:
                        var textNode = document.createTextNode("# place");
                        break;
                    case 1:
                        var textNode = document.createTextNode("player");
                        break;
                    case 2:
                        var textNode = document.createTextNode("score");
                        break;
                    case 3:
                        var textNode = document.createTextNode("word");
                        break;
                    case 4:
                        var textNode = document.createTextNode("time (secondes)");
                        break;
                    default:
                        var textNode = document.createTextNode("erreur");
                }
                td.appendChild(textNode);
                tr.appendChild(td);
            }
        } else {
            for (let j = 0; j < 5; j++) {
                const td = document.createElement("td");
                switch (j) {
                    case 0:
                        var textNode = document.createTextNode(i);
                        break;
                    case 1:
                        var player = prop_access(top, i+ ".player");
                        var textNode = document.createTextNode(player);
                        break;
                    case 2:
                        var score = prop_access(top, i + ".score");
                        var textNode = document.createTextNode(score);
                        break;
                    case 3:
                        var word = prop_access(top, i + ".data.word");
                        var textNode = document.createTextNode(word);
                        break;
                    case 4:
                        var time = prop_access(top, i + ".data.time");
                        var textNode = document.createTextNode(time);
                        break;
                    default:
                        var textNode = document.createTextNode("erreur");
                }
                td.appendChild(textNode);
                tr.appendChild(td);
            }
        }
        tbody.appendChild(tr);
    }
    table.appendChild(tbody);
    root.appendChild(table);
}

