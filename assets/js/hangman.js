import { generateTable } from './tablegenerate.js';
import { endChronometre } from './chrono.js';
import { saveScore } from './score.js';
import { decipher } from './secret.js';
import { loaderPageBis } from './loader.js';

// Le pendu 

//Lettre incorrecte et correctes
var letterDis = [];
var letterGood = [];

export function boutonClick() {
    document.getElementById('letters').addEventListener('click', event => {
        verifWord(event.path[0].id);
    });
}

//event pour modifier le dessin
document.addEventListener('keydown', event => {
    verifWord(event.key);
   
});


//Retourne valeur unique dans un Array
function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

// Affichage message divers time
function displayTime(id) {
    document.getElementById(id).style.display = "block";
    setTimeout(function () {
        document.getElementById(id).style.display = "none";
    }, 2000)
}

function verifWord(keyselect) {
    var keyselect = keyselect.toLowerCase();
    var url = location.hash.slice(1) || 'home';
    var unique = letterDis.filter(onlyUnique);
    // Action keydown active sur page1 seuelemtn
    if (url == 'page1' && unique.length < 12) {
        //Recupere le mot en session
        let wordE = sessionStorage.getItem("word");
        let word = decipher(wordE);
        // Transformation du mot en array
        let arrayWord = word.split('');
        // Alphabet en array pour verifier la saisie si la touche est un lettre 
        let alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
        // Si la lettre tapée n'appartient pas au tableau 
        if (arrayWord.includes(keyselect) == false && letterDis.includes(keyselect) == false && alphabet.includes(keyselect) == true) {
            // Nombre total de lettre mauvaise
            const count = letterDis.push(keyselect);
            // Dessin du pendu
            const hangmanPicture = document.getElementById('pendupict');
            if (count < 12) {
                // Perdu update dessin (grace au count)
                hangmanPicture.style.backgroundImage = 'url(./assets/images/p' + count + '.png)';
            } else {
                // Perdu fin de partie
                endChronometre();
                hangmanPicture.style.backgroundImage = 'url(./assets/images/perdu.png)';
                document.getElementById('letters').hidden = true;
            }
            hangmanPicture.style.visibility = "visible";
        } else if (letterDis.includes(keyselect) == true) {
            // Si la lettre est deja utilisée
            displayTime("letterutilise");
        } else if (arrayWord.includes(keyselect) == true) {
            // Si lettre est dans le mot
            generateTable(keyselect);
            // Nombre total de lettre bonne
            const countGood = letterGood.push(keyselect);
            var uniqueWordGood = letterGood.filter(onlyUnique);
            var uniqueWord = arrayWord.filter(onlyUnique);

            if (uniqueWord.length <= uniqueWordGood.length) {
                // Photo win
                const hangmanPicture = document.getElementById('pendupict');
                hangmanPicture.style.backgroundImage = 'url(./assets/images/win.png)';
                document.getElementById("resetpartie").style.display = "block";
                // Eteint chrono
                endChronometre();
                // Sauvegarde le score
                saveScore(unique.length, uniqueWord.length);
            }

        }
        // Désactivation du bouton sur la vue 
        if (alphabet.includes(keyselect) == true) {
            document.getElementById(keyselect.toUpperCase()).disabled = true;
        }
    } else if (url == 'page1' && unique.length == 12) {
        // Partie perdu
        displayTime("lost");
    }
}

export function resetValH() {
    // Loader
    loaderPageBis();
    //Reset variables
    letterDis = [];
    letterGood = [];
    // Reset disabled
    let alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
    alphabet.forEach(element => {
        document.getElementById(element.toUpperCase()).disabled = false;
        ;
    });
    // Reset picture
    const hangmanPicture = document.getElementById('pendupict');
    hangmanPicture.style.backgroundImage = 'url(./assets/images/white.png)';
    // Hide boutons
    // document.getElementById("resetpartie").style.display = "none";
    // Reset score
    document.getElementById("scorefinal").innerHTML = ""
}









